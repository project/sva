<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_views_accordion\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Functional tests for the Simple Views Accordion module.
 *
 * @group simple_views_acccordion
 */
final class SimpleViewsAccordionTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_views_accordion_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $type = NodeType::load('article');
    node_add_body_field($type);

    // Create articles.
    $this->createNode([
      'title' => 'article 1',
      'body' => [
        'value' => 'body 1',
        'format' => 'plain_text',
      ],
      'type' => 'article',
    ]);

    $this->createNode([
      'title' => 'article 2',
      'body' => [
        'value' => 'body 2',
        'format' => 'plain_text',
      ],
      'type' => 'article',
    ]);
  }

  /**
   * Accordion tests.
   */
  public function testSimpleViewsAccordion(): void {
    $this->drupalGet('simple-views-accordion-test');

    // Accordion titles are displayed.
    $this->assertSession()->pageTextContains('article 1');
    $this->assertSession()->pageTextContains('article 2');

    // Accordions not expanded by default.
    $this->assertSession()->pageTextNotContains('body 1');
    $this->assertSession()->pageTextNotContains('body 2');

    // Open first accordion item.
    $this->click('.simple-views-accordion.views-row:first-child details');
    $this->assertSession()->pageTextContains('body 1');
    $this->assertSession()->pageTextNotContains('body 2');

    // Open second accordion item.
    $this->click('.simple-views-accordion.views-row:last-child details');
    $this->assertSession()->pageTextContains('body 2');
    $this->assertSession()->pageTextNotContains('body 1');
  }

}
