(function (Drupal, once) {
  Drupal.behaviors.simpleViewsAccordion = {
    attach: function (context, settings) {
      once(
        'simpleViewsAccordion',
        'div.simple-views-accordion details',
        context
      ).forEach(function (D, _, A) {
        D.addEventListener(
          'toggle',
          (E) => D.open && A.forEach((d) => d != E.target && (d.open = false))
        );
      });
    },
  };
})(Drupal, once);
