<?php

/**
 * @file
 * Post update hooks for the simple_views_accordion module.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\views\ViewEntityInterface;

/**
 * Rename the style plugin ID to a shorter name.
 */
function simple_views_accordion_post_update_raname_plugin_id(array &$sandbox = NULL): void {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'view', function (ViewEntityInterface $view): bool {
    $is_update = FALSE;
    $displays = $view->get('display');
    foreach ($displays as &$display) {
      if (\array_key_exists('style', $display['display_options']) && $display['display_options']['style']['type'] === 'simple_views_accordion_simple_views_accordion') {
        $is_update = TRUE;
        $display['display_options']['style']['type'] = 'simple_views_accordion';
      }
    }

    if ($is_update) {
      $view->set('display', $displays);
    }

    return $is_update;
  });
}
